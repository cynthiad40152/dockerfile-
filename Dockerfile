FROM ubuntu:18.04

# Make sure the package repository is up to date.
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get install -y git
# Install a basic SSH server
RUN apt-get install -y openssh-server
RUN sed -i 's|session    required     pam_loginuid.so|session    optional     pam_loginuid.so|g' /etc/pam.d/sshd
RUN mkdir -p /var/run/sshd

# Install JDK 8 (latest edition)
RUN apt update
RUN apt install -y openjdk-8-jdk

# Add user jenkins to the image
RUN adduser --quiet jenkins
# Set password for the jenkins user (you may want to alter this).
RUN echo "jenkins:jenkins" | chpasswd

# Install ansible
RUN apt-get install -y ansible

# Standard SSH port
EXPOSE 22

RUN mkdir /home/jenkins/.ssh

ADD id_rsa.pub /home/jenkins/.ssh/authorized_keys

CMD ["/usr/sbin/sshd", "-D"]
